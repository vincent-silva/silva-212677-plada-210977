/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDateTime;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vince
 */
public class SistemaTest {

    @Test
    public void agregarEvento_normal() {
        Evento evento = new Evento();
        Sistema instancia = new Sistema();

        instancia.agregarEvento(evento);

        assertEquals(evento, instancia.getEventos().get(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void agregarEvento_nulo() {
        Evento evento = null;
        Sistema instancia = new Sistema();

        instancia.agregarEvento(evento);
    }

    @Test
    public void borrarEvento_normal() {
        Evento evento = new Evento();
        Sistema instancia = new Sistema();

        instancia.agregarEvento(evento);
        instancia.borrarEvento(evento);

        assertTrue(instancia.getEventos().isEmpty());
    }

    @Test
    public void getPaseos_normal() {
        Paseo paseo = new Paseo();
        Sistema instancia = new Sistema();

        instancia.agregarEvento(paseo);

        assertEquals(paseo, instancia.getPaseos().get(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPaseos_vacio() {
        Paseo paseo = null;
        Sistema instancia = new Sistema();

        instancia.agregarEvento(paseo);
    }

    @Test
    public void getPaseos_otroTipo() {
        Evento evento = new Evento();
        Sistema instancia = new Sistema();

        instancia.agregarEvento(evento);

        assertTrue(instancia.getPaseos().isEmpty());
    }

    @Test
    public void getComidas_normal() {
        Comida comida = new Comida();
        Sistema instancia = new Sistema();

        instancia.agregarEvento(comida);

        assertEquals(comida, instancia.getComidas().get(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getComidas_vacio() {
        Comida comida = null;
        Sistema instancia = new Sistema();

        instancia.agregarEvento(comida);
    }

    @Test
    public void getComidas_otroTipo() {
        Evento evento = new Evento();
        Sistema instancia = new Sistema();

        instancia.agregarEvento(evento);

        assertTrue(instancia.getComidas().isEmpty());
    }

    @Test
    public void getEventosDelDia_normal() {
        Evento evento = new Evento();
        LocalDateTime fecha = LocalDateTime.now();
        Sistema instancia = new Sistema();

        evento.setFecha(fecha);
        instancia.agregarEvento(evento);

        assertEquals(evento, instancia.getEventosDelDia(
                fecha.getYear(),
                fecha.getMonthValue(),
                fecha.getDayOfMonth()).get(0));
    }

    @Test
    public void getEventosDelDia_vacio() {
        Sistema instancia = new Sistema();
        LocalDateTime fecha = LocalDateTime.now();

        assertTrue(instancia.getEventosDelDia(
                fecha.getYear(),
                fecha.getMonthValue(),
                fecha.getDayOfMonth()).isEmpty());
    }

    @Test
    public void getEventosDelDia_diaMenos() {
        Evento evento = new Evento();
        LocalDateTime fecha = LocalDateTime.now();
        Sistema instancia = new Sistema();

        evento.setFecha(fecha.minusDays(1));
        instancia.agregarEvento(evento);

        assertTrue(instancia.getEventosDelDia(
                fecha.getYear(),
                fecha.getMonthValue(),
                fecha.getDayOfMonth()).isEmpty());
    }

    @Test
    public void getEventosDelDia_diaMas() {
        Evento evento = new Evento();
        LocalDateTime fecha = LocalDateTime.now();
        Sistema instancia = new Sistema();

        evento.setFecha(fecha.plusDays(1));
        instancia.agregarEvento(evento);

        assertTrue(instancia.getEventosDelDia(
                fecha.getYear(),
                fecha.getMonthValue(),
                fecha.getDayOfMonth()).isEmpty());
    }

    @Test
    public void getEventosParaNotificar_normal() {
        Evento evento = new Evento();
        LocalDateTime fecha = LocalDateTime.now();
        Sistema instancia = new Sistema();

        evento.setFecha(fecha);
        instancia.agregarEvento(evento);

        assertEquals(evento, instancia.getEventosParaNotificar(fecha).get(0));
    }

    @Test
    public void getEventosParaNotificar_fueraDeRangoSuperior() {
        Evento evento = new Evento();
        LocalDateTime fecha = LocalDateTime.now();
        Sistema instancia = new Sistema();
        evento.setFecha(fecha.plusHours(1).plusMinutes(1));
        instancia.agregarEvento(evento);

        assertTrue(instancia.getEventosParaNotificar(fecha).isEmpty());
    }

    @Test
    public void getEventosParaNotificar_fueraDeRangoInferior() {
        Evento evento = new Evento();
        LocalDateTime fecha = LocalDateTime.now();
        Sistema instancia = new Sistema();
        evento.setFecha(fecha.plusHours(1).plusMinutes(1));
        instancia.agregarEvento(evento);

        assertTrue(instancia.getEventosParaNotificar(fecha).isEmpty());
    }
}
