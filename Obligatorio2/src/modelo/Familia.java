/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Observable;
import javax.swing.ImageIcon;

/**
 *
 * @author vince
 */
public class Familia extends Observable {

    private String nombre;
    private ArrayList<Mascota> mascotas;
    private ArrayList<Persona> integrantes;
    private ArrayList<ImageIcon> imagenes;
    private String comentarios;

    public Familia() {
        this.nombre = "";
        this.mascotas = new ArrayList<>();
        this.integrantes = new ArrayList<>();
        this.imagenes = new ArrayList<>();
        this.comentarios = "";
    }

    public Familia(String unNombre) {
        this.nombre = unNombre;
        this.mascotas = new ArrayList<>();
        this.integrantes = new ArrayList<>();
        this.imagenes = new ArrayList<>();
        this.comentarios = "";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String unNombre) {
        this.nombre = unNombre;
    }

    public ArrayList<Mascota> getMascotas() {
        return mascotas;
    }

    public void setMascotas(ArrayList<Mascota> mascotas) {
        this.mascotas = mascotas;
    }

    public ArrayList<Persona> getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(ArrayList<Persona> integrantes) {
        this.integrantes = integrantes;
    }

    public void agregarIntegrante(Persona unIntegrante) {
        this.integrantes.add(unIntegrante);
        this.notifyObservers();
    }

    public void borrarIntegrante(Persona unIntegrante) {
        this.integrantes.remove(unIntegrante);
    }

    public void agregarMascota(Mascota unaMascota) {
        this.mascotas.add(unaMascota);
        this.notifyObservers();
    }

    public void borrarMascota(Mascota unaMascota) {
        this.mascotas.remove(unaMascota);
    }

    public ArrayList<ImageIcon> getImagenes() {
        return this.imagenes;
    }

    public void agregarImagen(ImageIcon img) {
        this.imagenes.add(img);
    }

    public void borrarImagen(ImageIcon img) {
        this.imagenes.remove(img);
    }

    public void setComentarios(String unComentario) {
        if (unComentario != null) {
            this.comentarios = unComentario;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public String getComentario() {
        return this.comentarios;
    }

    @Override
    public boolean equals(Object obj) {
        boolean respuesta = false;
        if (obj instanceof Familia) {
            respuesta = this.getNombre().toUpperCase().equals(((Familia) obj).getNombre().toUpperCase());
        }
        return respuesta;
    }

    /**
     * Se sobreescribe para ejecutar setChanged, ya que se espera que se
     * ejecuten juntos siempre dentro del modelo
     */
    @Override
    public void notifyObservers() {
        this.setChanged();
        super.notifyObservers();
    }
}
