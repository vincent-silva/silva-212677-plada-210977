/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obligatorio2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;
import modelo.Comida;
import modelo.Mascota;
import modelo.Persona;
import modelo.Sistema;
import modelo.Paseo;
import vistas.VentanaPrincipal;

/**
 *
 * @author vince
 */
public class Obligatorio2 {
    
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, UnsupportedLookAndFeelException, IllegalAccessException {
        Sistema modelo = new Sistema();
        Persona receptor = new Persona("Usuario de la aplicacion", 30, 'M');

        ArrayList<String> coord = new ArrayList<>();
        coord.add("-34.9002277,-56.1813389");
        coord.add("-34.8978343,-56.1772512");
        coord.add("-34.8978343,-56.1772512");
        coord.add("-34.8990046,-56.1810278");
        coord.add("-34.8957905,-56.181734");
        coord.add("-34.8934182,-56.1822914");
        coord.add("-34.8944766,-56.1750402");
        coord.add("-34.8968399,-56.1762857");
        
        Persona integrante1 = new Persona("Barak Obama", 54, 'M');
        Persona integrante2 = new Persona("Mandrake Wolf", 29, 'M');
        Persona integrante3 = new Persona("Carla Peterson", 29, 'M');
        Mascota mascota1 = new Mascota("Ataque", 10, 10, 10, "Comentario", "D:\\ORT\\Materias\\IngenieriaDeSoftware\\Pinder\\dogrunning.jpg");
        Mascota mascota2 = new Mascota("Tony", 10, 10, 10, "otro comentario", "imagen");
        Paseo paseo1 = new Paseo(integrante1, LocalDateTime.now(), "/"+coord.get(new Random().nextInt(coord.size()))+"/"+coord.get(new Random().nextInt(coord.size()))+"/@"+coord.get(new Random().nextInt(coord.size()))+"");
        paseo1.agregarMascota(mascota1);
        Paseo paseo2 = new Paseo(integrante2, LocalDateTime.now(), ""+coord.get(new Random().nextInt(coord.size()))+"/"+coord.get(new Random().nextInt(coord.size()))+"/"+coord.get(new Random().nextInt(coord.size()))+"/@"+coord.get(new Random().nextInt(coord.size()))+"");
        paseo2.agregarMascota(mascota2);
        Comida comida1 = new Comida(integrante2, LocalDateTime.now(), "DogChaw");
        comida1.agregarMascota(mascota1);
        Comida comida2 = new Comida(integrante3, LocalDateTime.now(), "Postre");
        comida2.agregarMascota(mascota2);

        modelo.agregarEvento(paseo1);
        modelo.agregarEvento(paseo2);
        modelo.agregarEvento(comida1);
        modelo.agregarEvento(comida2);

        modelo.getFamilia().agregarIntegrante(integrante1);
        modelo.getFamilia().agregarIntegrante(integrante2);
        modelo.getFamilia().agregarIntegrante(integrante3);
        modelo.getFamilia().agregarMascota(mascota1);
        modelo.getFamilia().agregarMascota(mascota2);

//        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        VentanaPrincipal ventana = new VentanaPrincipal(modelo);
        ventana.setVisible(true);
//      Se guarda llamado de ventana de pruebas para testing
//        VentanaDePruebas ventana2 = new VentanaDePruebas(modelo);
//        ventana2.setLocation(800, 0);
//        ventana2.setVisible(true);

        String email = JOptionPane.showInputDialog(null, "Ingrese el correo del destinatario de las pruebas");
        final ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
        ses.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                Alerta alerta = new Alerta(
                        modelo.getEventosParaNotificar(LocalDateTime.now()),
                        receptor,
                        email
                );
                alerta.notificar();
            }
        }, 0, 5, TimeUnit.MINUTES);
    }

}
