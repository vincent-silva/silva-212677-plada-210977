/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obligatorio2;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import modelo.Evento;
import modelo.Persona;
import vistas.VentanaDePruebas;

/**
 *
 * @author mjpla
 */
public class Alerta {

    private Persona receptor;
    private String email;
    private SystemTray bandeja;
    private ArrayList<Evento> envetos;

    public Alerta(ArrayList<Evento> unosEventos, Persona unReceptor, String unEmail) {
        if (SystemTray.isSupported()) {
            this.bandeja = SystemTray.getSystemTray();
        }
        this.envetos = unosEventos;
        this.receptor = unReceptor;
        this.email = unEmail;
    }

    public SystemTray getBandeja() {
        return bandeja;
    }

    public ArrayList<Evento> getEnvetos() {
        return envetos;
    }

    public void setBandeja(SystemTray bandeja) {
        this.bandeja = bandeja;
    }

    public void setEnvetos(ArrayList<Evento> envetos) {
        this.envetos = envetos;
    }

    /**
     * Metodo genera dos tipos de notificaciones: bandeja de notifiaciones y
     * mail El primero lo genera con java.awt y los mail con el complemento
     * javax.mail Para la generacion de candidatos a notificar, consulta en el
     * modelo por los eventos del dia y hora actual
     */
    public void notificar() {
        if (!this.getEnvetos().isEmpty()) {
            if (SystemTray.isSupported()) {
                Image image = Toolkit.getDefaultToolkit().createImage("recursos/Info_48px.png");
                TrayIcon trayIcon = new TrayIcon(image, "MensajeInfo");
                trayIcon.setImageAutoSize(true);
                trayIcon.setToolTip("Tiene eventos programados");
                try {
                    this.bandeja.add(trayIcon);

                } catch (AWTException ex) {
                    Logger.getLogger(VentanaDePruebas.class.getName()).log(Level.SEVERE, null, ex);
                }
                trayIcon.displayMessage("Programa de Mascotas caninas", "Ud. tiene " + this.getEnvetos().size() + " programados.", TrayIcon.MessageType.INFO);
                //Prueba de mail
                this.enviarMail(this.email, this.receptor, 9);
            }
        }
    }

    public void enviarMail(String direccion, Persona responsable, int cantidad) {
        final String username = "ingsoftobl2@gmail.com";
        final String password = "caballoblancodeartigas";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("ingsoftobl2@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(direccion));
            message.setSubject("Software de mascotas caninas");
            message.setText(responsable.toString() + " tienes " + cantidad + " eventos");

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
